<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }
require 'function.php';

$mahasiswa = query ("SELECT * FROM mahasiswa");
// jika tombol cari  di klik maka seluruh mahasiswa akan di timpa
if( isset($_POST["cari"]) ) {
   $mahasiswa = cari($_POST["keyword"]);

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>halaman admin</title>
<style>
    /* Global styles */

* {
    box-sizing: border-box;
  }
  
  body {
    margin: 0;
    font-family: 'Open Sans', sans-serif;
  }
  
  a {
    text-decoration: none;
  }

  .container {
    max-width: 1200px;
    margin: 0 auto;
    padding: 0 20px;
  }
  
  /* Header styles */
  
  header {
    background-color: #333;
    color: #fff;
    padding: 20px;
    text-align: center;
  }
  
  h1 {
    font-size: 36px;
    margin-top: 50px;
    margin-bottom: 20px;
  }
  
  /* Navigation styles */
  
  nav {
    background-color: #4CAF50;
    color: #fff;
    padding: 10px 20px;
    text-align: center;
  }
  
  nav a {
    color: #fff;
    font-weight: bold;
    margin-right: 20px;
  }
  
  /* Table styles */
  
  table {
    width: 100%;
    border-collapse: collapse;
    margin-top: 50px;
  }
  
  th, td {
    padding: 12px;
    text-align: left;
    border-bottom: 1px solid #ddd;
  }
  
  th {
    background-color: #f2f2f2;
  }
  
  tr:nth-child(even) {
    background-color: #f2f2f2;
  }
  
  /* Form styles */
  
  form {
    margin-top: 50px;
  }
  
  form label {
    display: block;
    margin-bottom: 5px;
  }
  
  form input[type="text"], form input[type="email"] {
    padding: 10px;
    margin-bottom: 20px;
    width: 100%;
    border-radius: 5px;
    border: none;
    background-color: #f2f2f2;
  }
  
  form input[type="submit"] {
    background-color: #4CAF50;
    color: #fff;
    padding: 10px 20px;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    transition: background-color 0.3s ease-in-out;
  }
  
  form input[type="submit"]:hover {
    background-color: #35a04c;
  }
  
  /* Footer styles */
  
  footer {
    background-color: #333;
    color: #fff;
    padding: 20px;
    text-align: center;
  }
  
  @media screen and (min-width: 768px) {
    /* Table styles */
    
    table {
      width: 90%;
      margin: 50px auto;
    }
    
    /* Form styles */
    
    form {
      width: 60%;
      margin: 50px auto;
    }
  }
      .kotak-link {
  display: inline-block;
  padding: 10px 20px;
  background-color:  aqua;
  border: 1px solid #ccc;
  text-decoration: none;
  color: black;
}
    .kotak-link2 {
  display: inline-block;
  padding: 10px 20px;
  background-color:  black;
  border: 1px solid #ccc;
  text-decoration: none;
  color: white;
}
    .kotak-link3 {
  display: inline-block;
  padding: 10px 20px;
  background-color:  green;
  border: 1px solid #ccc;
  text-decoration: none;
  color: white;
}
</style>
</head>
<body>
    <h1 align="middle">Daftar Siswa</h1>
    <a class="kotak-link" href="logout.php">log out</a> <br>
    <a class="kotak-link2" href="ubah_pengumuman.php">buat/edit pengumuman</a> <br>
    <a class="kotak-link3" href="registrasi.php">tambah admin</a>
 <br>
 <br>
<form action="" method="post">

<input type="text" name="keyword" size="30" autofocus placeholder="masukkan keyword pencarian" autocomplete="off">
<button type="submit" name="cari">cari</button>

</form> <br>
<table border="1" cellpadding="10" cellcpasing="0">
    <tr>
        <th>no.</th>
        <th>NAMA</th>
        <th>NIK</th>
        <th>ALAMAT</th>
        <th>JENIS KELAMIN</th>
        <th>NO_HP</th>
        <th>EMAIL</th>
        <th>AKSI</th>
    </tr>
    <?php $i = 1;  ?>
    <?php foreach ($mahasiswa as $row) : ?>
    <tr >
        <td> <?php echo $i; ?></td>
        <td><?php echo $row["nama"] ?></td>
        <td><?php echo $row["nisn"] ?></td>
        <td><?php echo $row["alamat"] ?></td>
        <td><?php echo $row["jenis_kelamin"] ?></td>
        <td><?php echo $row["no_hp"] ?></td>
        <td><?php echo $row["email"] ?></td>
        <td><a href="ubah.php?id=<?= $row["id"]; ?>">ubah</a> |
          <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin akan menghapus data?')">hapus</a>
        </td>
        
    </tr>
    <?php  $i++;  ?>
    <?php endforeach;  ?>
    </table>
</body>
</html>