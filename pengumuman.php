<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }
require 'function.php';

$mhs = query ("SELECT * FROM pengumuman")[0];

?>
<!DOCTYPE html>
<html>
<head>
  <title>Halaman Pengumuman</title>
  <link rel="stylesheet" type="text/css" href="pengumuman.css">
  <style>
       /* Style untuk navbar */
       nav {
      background-color: #333;
      overflow: hidden;
    }

    nav a {
      float: left;
      display: block;
      color: white;
      text-align: center;
      padding: 8px 16px;
      text-decoration: none;
    }

    /* Efek hover pada link navbar */
    nav a:hover {
      background-color: #ddd;
      color: black;
    }

    /* Efek aktif pada link navbar */
    nav a.active {
      background-color: #4CAF50;
      color: white;
    }
   .kotak-link {
  display: inline-block;
  padding: 10px 20px;
  background-color:  grey;
  border: 1px solid #ccc;
  text-decoration: none;
  color: white;
}
  </style>
</head>
<body>
<a  class="kotak-link" href="siswa.php">kembali</a>
 <br>
  <header>
    <h1>Pengumuman Terbaru</h1>
  </header>
  <main>
    <div class="announcement">
      <h2><?= $mhs["t1"]  ?></h2>
      <p><?= $mhs["p1"]  ?></p>
    </div>
    <div class="announcement">
      <h2><?= $mhs["t2"]  ?></h2>
      <p><?= $mhs["p2"]  ?></p>
    </div>
    <div class="announcement">
      <h2><?= $mhs["t3"]  ?></h2>
      <p><?= $mhs["t3"]  ?></p>
    </div>
  </main>
  <footer>
    <p> &copy; 2023 Halaman Pengumuman</p>
  </footer>
</body>
</html>
