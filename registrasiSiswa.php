<?php
 require 'function.php';

if ( isset($_POST["register"]) ){

    if( registrasiSiswa($_POST) > 0 ){
        echo "<script>
        alert('DATA ANDA TELAH TERVERIFIKASI');
        window.location.href = 'logins.php';
        </script>";
      
    } else {
        echo mysqli_error($conn);
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registrasi</title>
    <style>
        /* Body Styles */
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 20px;
        }
        
        /* Container Styles */
        .container {
            max-width: 400px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        
        /* Heading Styles */
        h1 {
            text-align: center;
            margin-top: 0;
        }
        
        /* Form Styles */
        form ul {
            list-style: none;
            padding: 0;
        }
        
        form li {
            margin-bottom: 10px;
        }
        
        label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold;
        }
        
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 8px;
            border-radius: 3px;
            border: 1px solid #ccc;
        }
        
        button[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        
        button[type="submit"]:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
<h1>BUAT USERNAME DAN PASSWORD ANDA!</h1>    
 
<form action="" method="post">

<ul>
    <li>
        <label for="username">username :</label>
        <input type="text" name="username" id="username">
    </li>
    <li>
    <label for="password">password :</label>
        <input type="password" name="password" id="password">
    </li>
    <li>
    <label for="password2">konfirmasi password :</label>
        <input type="password" name="password2" id="password2">
    </li>
    <li>
        <button type="submit" name="register">verify</button>
    </li>
</ul>

</form>

</body>
</html>