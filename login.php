<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }

require 'function.php';


if( isset($_POST["login"])){

    $username = $_POST["username"];
    $password = $_POST["password"];

$result = mysqli_query($conn , "SELECT * FROM user WHERE username = '$username'");

if( mysqli_num_rows($result) === 1){
    
        // cek password
        $row = mysqli_fetch_assoc($result);
        if( password_verify($password, $row["password"]) ){
     
            
            header("location: admin.php");
            
            exit;
        }
}   $error = true;
   }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Login Admin</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
        }

        h1 {
            text-align: center;
            margin-top: 50px;
        }

        .login-container {
            max-width: 300px;
            margin: 0 auto;
            background-color: #fff;
            padding: 40px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            animation: slide-up 0.6s ease;
        }

        .login-container ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .login-container li {
            margin-bottom: 15px;
        }

        .login-container label {
            display: block;
            font-weight: bold;
        }

        .login-container input[type="text"],
        .login-container input[type="password"] {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        .login-container input[type="checkbox"] {
            display: inline-block;
            vertical-align: middle;
        }

        .login-container button[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
            font-size: 16px;
        }

        .login-container button[type="submit"]:hover {
            background-color: #45a049;
        }

        /* Animasi slide-up */
        @keyframes slide-up {
            0% {
                transform: translateY(100px);
                opacity: 0;
            }
            100% {
                transform: translateY(0);
                opacity: 1;
            }
        }
    </style>
</head>

<body style="background-color: aqua;">
    <h1>Halaman Login ADMIN!</h1>
    <div class="login-container">
        <form action="" method="post">
            <ul>
                <li>
                    <label for="username">Username:</label>
                    <input type="text" name="username" id="username" autocomplete="off">
                </li>
                <li>
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" autocomplete="off">
                </li>
                <li>
                    <input type="checkbox" name="remember" id="remember">
                    <label for="remember">Ingatkan saya</label>
                </li>
                <li>
                    <button type="submit" name="login">Login</button>
                </li>
            </ul>
        </form>
    </div>
</body>

</html>
