<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }
require 'function.php';

$mhs = query ("SELECT * FROM pengumuman")[0];
if (isset($_POST["submit"])) {

  if (ubahp($_POST) > 0) {
    echo " <script> alert('data berhasi diubah'); 
    window.location.href = 'admin.php';
    </script>
    ";
  } else {
    echo "<script> alert('data gagal diubah');
    window.location.href = 'admin.php';
    </script>
    ";
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <style>
      .lihat {
    margin-top: 50px;
    padding: 20px;
    background-color: #333;
    border-radius: 5px;
  }

  .lihat header h1 {
    color: #333;
    font-size: 24px;
    margin-bottom: 20px;
  }

  .lihat main {
    display: flex;
    flex-direction: column;
  }

  .lihat .announcement {
    margin-bottom: 20px;
    padding: 10px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  }

  .lihat .announcement h2 {
    color: #333;
    font-size: 20px;
    margin-bottom: 10px;
  }

  .lihat .announcement p {
    color: #666;
    margin-bottom: 10px;
  }

  .lihat .announcement .date {
    color: #999;
    font-size: 14px;
  }

  .lihat footer {
    margin-top: 20px;
    text-align: center;
    color: #999;
    font-size: 14px;
  }
   .kotak-link {
  display: inline-block;
  padding: 10px 20px;
  background-color:  grey;
  border: 1px solid #ccc;
  text-decoration: none;
  color: white;
}
  </style>
  <title>ubah data</title>
</head>

<body>
  <h1>Ubah pengumuman</h1>
<a class="kotak-link" href="admin.php">kembali</a>

  <form action="" method="post">
    <ul>
      <li>
        <h6>Bagian 1</h6>
        <label for="t1">Tanggal:</label>
        <input type="text" name="t1" id="t1" size="100" value="<?= $mhs["t1"] ?>">
      </li>
      <li>
        <label for="p1">Data:</label>
        <input type="text" name="p1" id="p1" value="<?= $mhs["p1"] ?>">
      </li>

      <li>
        <h6>Bagian 2</h6>
        <label for="t2">Tanggal:</label>
        <input type="text" name="t2" id="t2" value="<?= $mhs["t2"] ?>">
      </li>
      <li>
        <label for="p2">Data:</label>
        <input type="text" name="p2" id="p2" value="<?= $mhs["p2"] ?>">
      </li>

      <li>
        <h6>Bagian 3</h6>
        <label for="t3">Tanggal:</label>
        <input type="text" name="t3" id="t3" value="<?= $mhs["t3"] ?>">
      </li>
      <li>
        <label for="p3">Data:</label>
        <input type="text" name="p3" id="p3" value="<?= $mhs["p3"] ?>">
      </li>

      <li>
        <button type="submit" name="submit">Ubah</button>
      </li>
    </ul>
  </form>
  <hr>
  <h1>hasil pengumuman</h1>
  <div class="lihat">
  <header>
    <h1>Pengumuman Terbaru</h1>
  </header>
  <main>
    <div class="announcement">
      <h2><?= $mhs["t1"]  ?></h2>
      <p><?= $mhs["p1"]  ?></p>
    </div>
    <div class="announcement">
      <h2><?= $mhs["t2"]  ?></h2>
      <p><?= $mhs["p2"]  ?></p>
    </div>
    <div class="announcement">
      <h2><?= $mhs["t3"]  ?></h2>
      <p><?= $mhs["t3"]  ?></p>
    </div>
  </main>
  <footer>
    <p> &copy; 2023 Halaman Pengumuman</p>
  </footer>
  </div>
</body>

</html>